create table AppDetails(
   id CHAR(160) NOT NULL,
   description TEXT NOT NULL,
   Name TEXT  NOT NULL,
   Formats TEXT ,
   PRIMARY KEY (id)
);
CREATE TABLE InputDetails(
	id VARCHAR(500),
    AppId CHAR(160) NOT NULL,
    Description TEXT,
    label TEXT,
    Inputtype TEXT,
    FormType TEXT,
    htmldata TEXT,
    PRIMARY KEY (id),
    FOREIGN KEY (AppId)
        REFERENCES AppDetails(id)
);
CREATE TABLE Arguments(
        	id VARCHAR(500),
        	Name TEXT,
        	isDefault Boolean,
        	Display TEXT,
        	value TEXT,
        	InputDetailId VARCHAR(500),
        	PRIMARY KEY (id),
        	FOREIGN KEY (InputDetailId)
    		REFERENCES InputDetails(id)
);
CREATE TABLE igbfileformats(
        	id VARCHAR(500),
        	filetype TEXT,
        	PRIMARY KEY (id)
);
